#!/usr/bin/python

URL="http://wiki.dbpedia.org/Downloads38"

import BeautifulSoup as beauty
import requests
r = requests.request("GET", URL)
c = r.content
soup = beauty.BeautifulSoup(c)
links = soup.findAll('a')
safe_links = map(lambda y: y['href'], \
 filter(lambda x: x.has_key('href') and x['href'].count('/en/') > 0, links) )

for l in safe_links:
    print l#['href']
